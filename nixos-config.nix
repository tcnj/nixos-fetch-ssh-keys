{ keysUrl, extraUsers }:
{ pkgs, lib, ... }:
{
  services.openssh.authorizedKeysFiles = [ "/run/fetched-ssh-keys/%u" ];

  systemd.services.fetch-ssh-keys = {
    wantedBy = [ "multi-user.target" ];
    after = [ "network.target" ];
    before = [ "sshd.service" ];

    serviceConfig = {
      Type = "simple";
    };

    environment = {
      EXTRA_USERS = lib.concatStringsSep " " extraUsers;
    };

    script = ''
      do_fetch() {
        ${pkgs.curl}/bin/curl -sSL "${keysUrl}" -o /run/fetched-ssh-keys/root && \
          echo Fetch successful!
      }

      while true; do
        mkdir -m 0755 -p /run/fetched-ssh-keys/
        echo "Fetching SSH keys from ${keysUrl}..."

        wait_time=2
        until do_fetch
        do
          echo "Fetch failed, backing off for $wait_time seconds..."
          if [ $wait_time -lt 10 ]; then
            wait_time=$(( wait_time + 1 ))
          fi
          sleep $wait_time
          echo "Retrying..."
        done

        for extra_user in $EXTRA_USERS; do
          cp /run/fetched-ssh-keys/root /run/fetched-ssh-keys/$extra_user
        done

        echo "Resting for five minutes"
        sleep 300
      done
    '';
  };
}
