{
  description = "NixOS installation media that allows SSH.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
    flake-parts.url = "github:hercules-ci/flake-parts";
  };

  outputs = { self, nixpkgs, flake-parts }@inputs:
    let
      lib = import ./lib.nix inputs;
    in
    flake-parts.lib.mkFlake { inherit inputs; } {
      flake = {
        inherit lib;
      };
      systems = [
        "x86_64-linux"
        "aarch64-linux"
      ];
      perSystem = { system, ... }: {
        packages.iso = lib.makeInstallationMedia { inherit system; };
      };
    };
}
