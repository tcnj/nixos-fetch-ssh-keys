{ nixpkgs, ... }:
let
  defaultKeysUrl = "https://gitlab.com/tcnj.keys";
  lib = nixpkgs.lib;
in
rec {
  makeNixosModule =
    { keysUrl ? defaultKeysUrl
    , extraUsers ? [ "nixos" ]
    }:
    import ./nixos-config.nix {
      inherit keysUrl extraUsers;
    };
  makeInstallationMedia =
    { keysUrl ? defaultKeysUrl
    , system ? "x86_64-linux"
    , extraUsers ? [ "nixos" ]
    }:
    let
      nixosConfig = nixpkgs.lib.nixosSystem {
        inherit system;
        modules = [
          "${nixpkgs}/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix"
          (makeNixosModule { inherit keysUrl extraUsers; })
          {
            boot.loader.timeout = lib.mkForce 1;
            isoImage.squashfsCompression = "gzip -Xcompression-level 1";
          }
        ];
      };
    in
    nixosConfig.config.system.build.isoImage;
}
